﻿using Autofac;
using OpenQA.Selenium;
using ShareGainPractice.DAL.ApiAccsess.ApiRoutes;
using ShareGainPractice.ShareGainApi;
using ShareGainPractice.ShareGainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ShareGainPractice.ContainerInitializedFolder
{
    public  class ContainerInitialized
    {
        public  IContainer ContainerConfigure(IWebDriver driver, HttpClient client = null)
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<UnitsAvailableApi>().As<IUnitsAvailableApi>();
            builder.RegisterType<UnitsAvailableUi>().OnActivating(e => e.Instance.Driver = driver).AsSelf();
            builder.RegisterType<ApiRouteAggregate>().As<IApiRouteAggregate>();
            var container = builder.Build();

            return container;
        }
    }
}
