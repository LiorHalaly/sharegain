﻿using OpenQA.Selenium;
using ShareGainPractice.Extensions;
using ShareGainPractice.ShareGainObjects;
using ShareGainPractice.TestsInternals;
using System;

namespace ShareGainPractice.ShareGainApi
{
    public class UnitsAvailableUi : ApplicationFactory, IUnitsAvailableUi
    {
       
        public IWebDriver Driver { get; set; }

        #region Locators
        private readonly By UserNameExp = By.XPath("//input[@id= 'username']");
        private readonly By PasswordExp = By.XPath("//input[@id= 'password']");
        private readonly By LogInButton = By.XPath("//button[@class= 'btn btn-theme btn-block']");
        private readonly By UnitsAvailable = By.XPath("//pbl-ngrid-cell[@id='unitsAvailable']");
        #endregion


        public IUnitsAvailableUi LogIn()
        {
            // log in 
            Driver.GetElement(UserNameExp).SendKeys("AutomationTest_Lender");
            Driver.GetElement(PasswordExp).SendKeys("Lender123!");
            Driver.GetElement(LogInButton).Click();
            return this;
        }
        public string GetUnitsAvailableValue()
        {
            // my protfolio
            var result = Driver.GetElement(UnitsAvailable).Text;
            return result;
        }
    }
}
