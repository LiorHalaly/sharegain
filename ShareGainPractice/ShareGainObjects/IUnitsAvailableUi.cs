﻿using OpenQA.Selenium;

namespace ShareGainPractice.ShareGainApi
{
    public interface IUnitsAvailableUi
    {
        IWebDriver Driver { get; set; }

        string GetUnitsAvailableValue();
        IUnitsAvailableUi LogIn();
    }
}