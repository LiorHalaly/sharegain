﻿using ShareGainPractice.ShareGainObjects.Dto_s;
using System.Threading.Tasks;

namespace ShareGainPractice.ShareGainObjects
{
    public interface IUnitsAvailableApi
    {
        Task<GetSettingsResponce> GetSettings();
        //void PostSettings(GetSettingsResponce getSettingsResponce, int Presentage);
    }
}