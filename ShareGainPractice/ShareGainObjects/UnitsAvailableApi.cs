﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using ShareGainPractice.DAL.ApiAccsess;
using ShareGainPractice.DAL.ApiAccsess.ApiRoutes;
using ShareGainPractice.ShareGainObjects.Dto_s;
using ShareGainPractice.TestsInternals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ShareGainPractice.ShareGainObjects
{
    public class UnitsAvailableApi : ApplicationFactory, IUnitsAvailableApi
    {
        public HttpClient client = new HttpClient();


        IApiRouteAggregate _apiRouteAggregate;

        public UnitsAvailableApi(IApiRouteAggregate apiRouteAggregate)
        {
            _apiRouteAggregate = apiRouteAggregate;
        }

        public string baseApiUrl = @"https://api-test.sharegain.com/sg-lender/lending-terms/webapi/";

        public async Task<GetSettingsResponce> GetSettings()
        {
            client.BaseAddress = new Uri(baseApiUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "Bearer 79b333824f0cca834c97520bfc002ebc");
            client.DefaultRequestHeaders.Add("X-SG-USER", "292e954e-bb45-4c7e-8839-f77ce26bbc1e");
            var route = _apiRouteAggregate.GetSettingsRequestEntry();
            var response = await client.GetAsync(route);
            response.EnsureSuccessStatusCode();
            var json = response.Content.ReadAsStringAsync().Result;
           var iii = JsonConvert.SerializeObject(response);
            var kk = JsonConvert.DeserializeObject<GetSettingsResponce>(json);
            return kk;
        }


        public void PostSettings(Rootobject getSettingsResponce, int Presentage)
        {
            getSettingsResponce.selfSettings.lendingLimit = Presentage;
            var content = JsonConvert.SerializeObject(getSettingsResponce);
            var stringContent = new StringContent(content);
            var route = _apiRouteAggregate.PostSettingsRequestEntry();
            var response = client.PostAsync(route, stringContent);
        }
    }
}
