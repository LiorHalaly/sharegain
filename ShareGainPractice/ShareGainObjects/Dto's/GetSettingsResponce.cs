﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareGainPractice.ShareGainObjects.Dto_s
{
    public class GetSettingsResponce
    {
        public Selfsettings selfSettings { get; set; }
        public Securitytypesapproval securityTypesApproval { get; set; }
        public Tradetypesapproval tradeTypesApproval { get; set; }
        public Manuallyapprovedborrower[] manuallyApprovedBorrowers { get; set; }
    }
        public class Rootobject
        {
            public Selfsettings selfSettings { get; set; }
            public object mergedSettings { get; set; }
        }

        public class Selfsettings
        {
            public Securitytypesapproval securityTypesApproval { get; set; }
            public int lendingLimit { get; set; }
            public object[] manualLendingLimits { get; set; }
            public float minimumLendingReturn { get; set; }
            public int defaultMinimumLendingLimit { get; set; }
            public Tradetypesapproval tradeTypesApproval { get; set; }
            public string priorApprovalType { get; set; }
            public string approvedBorrowersType { get; set; }
            public Manuallyapprovedborrower[] manuallyApprovedBorrowers { get; set; }
            public float portfolioValueLimit { get; set; }
            public float feeSplit { get; set; }
        }

        public class Securitytypesapproval
        {
            public bool Stocks { get; set; }
            public bool ETFs { get; set; }
            public bool Bonds { get; set; }
        }

        public class Tradetypesapproval
        {
            public bool Hold { get; set; }
            public bool LoanWithRecall { get; set; }
            public bool LoanWithoutRecall { get; set; }
        }

        public class Manuallyapprovedborrower
        {
            public string id { get; set; }
            public string name { get; set; }
            public bool approved { get; set; }
        }

    
}
