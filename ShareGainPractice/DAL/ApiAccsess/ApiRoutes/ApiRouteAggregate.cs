﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareGainPractice.DAL.ApiAccsess.ApiRoutes
{
    public class ApiRouteAggregate : IApiRouteAggregate
    {
        public string PostSettingsRequestEntry()
        {
            return "lender/v2/settings/292e954e-bb45-4c7e-8839-f77ce26bbc1e";
        }

        public string GetSettingsRequestEntry()
        {
            return "lender/v2/settings/292e954e-bb45-4c7e-8839-f77ce26bbc1e";
        }

    }
}
