﻿namespace ShareGainPractice.DAL.ApiAccsess.ApiRoutes
{
    public interface IApiRouteAggregate
    {
        string GetSettingsRequestEntry();
        string PostSettingsRequestEntry();
    }
}