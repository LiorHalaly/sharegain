﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShareGainPractice.TestsInternals
{
    public interface IApplicationFactory
    {
        T ChangeContext<T>(string application) where T : class;
    }
}
