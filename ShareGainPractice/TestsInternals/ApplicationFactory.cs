﻿using Autofac;
using AventStack.ExtentReports.Configuration;
using OpenQA.Selenium;
using ShareGainPractice.ContainerInitializedFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace ShareGainPractice.TestsInternals
{
    public class ApplicationFactory : IApplicationFactory
    {
        protected IWebDriver _driver;
        protected HttpClient _client;
        private string _application;

        public ApplicationFactory(IWebDriver driver)
        {
            _driver = driver;
        }

        public ApplicationFactory() { }

        public void OpenApplication(string application)
        {
            _application = application;
            _driver.Navigate().GoToUrl(_application);
            _driver.Manage().Window.Maximize();
        }

        public T ChangeContext<T>(string application = null) where T : class
        {
            if (!typeof(T).Name.Contains("Api"))
            {
                OpenApplication(application);
            }

            var containerInit = new ContainerInitialized();
            var container = containerInit.ContainerConfigure(_driver);
            var workingClass = (T)container.Resolve(typeof(T));
            return workingClass;
        }
    }
}
