﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ShareGainPractice.Extensions
{
    public static class WebDriverExtension
    {
        public static IWebElement GetElement(this IWebDriver driver, By by, int fromSeconds = 40)
        {
            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(fromSeconds));

            var element = wait.Until(d =>
            {
                try
                {
                    for (int i = 0; i < fromSeconds; i++)
                    {
                        var elementToBeDisplayed = driver.FindElement(by);                      

                        if (elementToBeDisplayed.Displayed)
                        {
                            return elementToBeDisplayed;
                        }
                    }
                    return null;
                }             
                catch (Exception ex)
                {
                    return null;
                }
            });
            return element;
        }
          
    }
}
