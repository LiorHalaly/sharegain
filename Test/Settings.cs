﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using ShareGainPractice.ShareGainApi;
using ShareGainPractice.ShareGainObjects;
using ShareGainPractice.TestsInternals;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace Test
{
    [TestFixture]
    public class Settings : TestSuitBase
    {
        [Test]
        public void SettingsE2E()
        {
            var settingResult = new ApplicationFactory()
                 .ChangeContext<IUnitsAvailableApi>()
                 .GetSettings().Result;

            var resultFromUi = new ApplicationFactory(driver)
                .ChangeContext<UnitsAvailableUi>(shareGainSiteUrl)
                .LogIn()
                .GetUnitsAvailableValue();

            //var settingResult = new ApplicationFactory()
            //      .ChangeContext<IUnitsAvailableApi>()
            //      .GetSettings();

            //new ApplicationFactory()
            //      .ChangeContext<IUnitsAvailableApi>()
            //      .PostSettings(settingResult, 40);

            var afterChange = new ApplicationFactory()
                  .ChangeContext<IUnitsAvailableApi>()
                  .GetSettings();

            //int actoal = afterChange.selfsettings.lendingLimit;

            //Assert.IsTrue(actoal == 40);
        }
    }
}
